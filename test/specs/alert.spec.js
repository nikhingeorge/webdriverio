import { expect as expectChai } from 'chai'
import AlertPage from '../../pageobjects/pages/alert.page';



describe('Alert test', () => {
    it('Test JS Alert', async () => {
        await AlertPage.open()

        await AlertPage.jsAlertButton.waitAndClick();
        // await browser.pause(3000)
        const isAlertOpen = await browser.isAlertOpen();
        if (isAlertOpen) {
            const alertText = await browser.getAlertText(); //get the alert text
            console.log('The alert text is: ', alertText) //logs the text to console
            await browser.acceptAlert(); //accepts the alert popup
        }

        // expectChai(await AlertPage.resultText.getText()).to.equal('You successfully clicked an alert');
        expect(await AlertPage.resultText.getText()).toEqual('You successfully clicked an alert')
        await browser.pause(5000);

    })

    xit('Test JS Prompt Alert', async () => {
        const textToPass = 'browserstack'
        await AlertPage.promptAlertButton.waitAndClick();
        // await browser.pause(3000)
        const isAlertOpen = await browser.isAlertOpen();
        if (isAlertOpen) {
            const alertText = await browser.getAlertText();
            await browser.sendAlertText(textToPass);
            console.log('The alert text is: ', alertText);
            await browser.acceptAlert();
        }

        // expectChai(await AlertPage.resultText.getText()).to.equal('You successfully clicked an alert');
        expect(await AlertPage.resultText.getText()).toEqual(`You entered: ${textToPass}`)
        await browser.pause(5000);

    })

    xit('Test JS Confirm Alert', async () => {
        await AlertPage.confirmAlertButton.waitAndClick();
        const isAlertOpen = await browser.isAlertOpen();
        await browser.pause(2000);
        if (isAlertOpen) {
            const alertText = await browser.getAlertText();
            console.log('The alert text is: ', alertText);
            await browser.dismissAlert();
        }

        // expectChai(await AlertPage.resultText.getText()).to.equal('You successfully clicked an alert');
        expect(await AlertPage.resultText.getText()).toEqual('You clicked: Cancel')
        await browser.pause(5000);
    })
})