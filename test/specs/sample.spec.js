describe('Sample test', function () {
    it("Search in Google", async() => {
        await browser.url("https://www.google.com/");
        await $('[name="q"]').waitForDisplayed();
        await $('[name="q"]').setValue("Webdriverio");
        await browser.keys("Enter");
        await browser.pause(5000);
    })
})