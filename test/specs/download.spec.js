import DownloadPage from '../../pageobjects/pages/download.page';

const fs = require('fs');
const path = require('path');
const dir = './target';


describe('Download test', () => {
    before(() => {
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
    })
    it('Download file test', async () => {
        await DownloadPage.open()
        await DownloadPage.downloadLinkMyScreenshot.waitAndClick()
        await browser.pause(3000);

        // Download file to local system
        if (process.env.ENVIRONMENT == "browserstack") {
            const base64data = await browser.executeScript(`browserstack_executor: {"action": "getFileContent", "arguments": {"fileName": "test-file.txt"}}`, []);
            const data = Buffer.from(base64data, 'base64');
            fs.writeFile([dir, 'test-file.txt'].join('/'), data, function (err) { if (err) throw err; });
        }


    })
})