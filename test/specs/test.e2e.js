/**
 * test with page objects
 */
import LoginPage from '../../pageobjects/pages/login.page';
import SecurePage from '../../pageobjects/pages/secure.page';

describe('My Login application', () => {
    it('should login with valid credentials', async () => {
        await LoginPage.open()

        await LoginPage.login(process.env.USER_NAME, process.env.PASSWORD)
        // await expect(SecurePage.flashAlert).toBeExisting()
        await expect(SecurePage.flashAlert).toHaveTextContaining(
            'You logged into a secure area!')
    })
})

