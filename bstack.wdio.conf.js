import baseConfig from './wdio.conf';

/**
 *
 * Webdriverio Configuration for cross-browser testing for Firefox, Safari, and Edge (Latest Versions)
 */

const override = {
    user: 'nikhinjosephgeor_xoMtUK',
    key: 'XeQdv9xTWbbfVYSmFqip',


    services: [
        ['browserstack', {
            testObservability: true,
            testObservabilityOptions: {
                projectName: "Testing framework",
                buildName: "The static build job name goes here e.g. Nightly regression"
            },
            browserstackLocal: true
        }]
    ],
    //
    // ============
    // Capabilities
    // ============
    maxInstances: 2,
    //
    // Or set a limit to run tests with a specific capability.
    maxInstancesPerCapability: 2,
    //
    capabilities: [
        // {
        //     maxInstances: 5,
        //     browserName: 'firefox',
        //     browserVersion: 'latest'
        // },
        // {
        //     maxInstances: 5,
        //     browserName: 'safari',
        //     browserVersion: 'latest'
        // },
        {
            maxInstances: 5,
            browserName: 'chrome',
            browserVersion: 'latest'
        },
    ],
    // Default timeout for all waitForXXX commands.
    waitforTimeout: 25000,
};
const outputConfig = {...baseConfig.config, ...override };
exports.config = outputConfig;
