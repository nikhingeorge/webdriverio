

import Page from './page'

/**
 * sub page containing specific selectors and methods for a specific page
 */
const SELECTORS = {
    jsAlertButton: 'button[onclick="jsAlert()"]',
    confirmAlertButton: 'button[onclick="jsConfirm()"]',
    promptAlertButton: 'button[onclick="jsPrompt()"]',
    resultText: '#result'
};
const MULTISELECTORS = {};
class LoginPage extends Page {
    constructor() {
        super();
        this.installGetters({ selectors: SELECTORS, type: 'single' });
        this.installGetters({ selectors: MULTISELECTORS, type: 'multi' });
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    async open() {
        return super.open('javascript_alerts');
    }
}

export default new LoginPage();
