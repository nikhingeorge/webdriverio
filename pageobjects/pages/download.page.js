

import Page from './page'

/**
 * sub page containing specific selectors and methods for a specific page
 */
const SELECTORS = {
    downloadLinkMyScreenshot: '[href="download/LambdaTest.txt"]'
};
const MULTISELECTORS = {};
class LoginPage extends Page {
    constructor() {
        super();
        this.installGetters({ selectors: SELECTORS, type: 'single' });
        this.installGetters({ selectors: MULTISELECTORS, type: 'multi' });
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    async open() {
        return super.open('download');
    }
}

export default new LoginPage();
