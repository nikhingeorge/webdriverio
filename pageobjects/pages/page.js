/**
* main page object containing all methods, selectors and functionality
* that is shared across all page objects
*/
import BaseComponent from '../components/base-component';

const SELECTORS = {
    password: '#password'
}
const MULTISELECTORS = {}

export default class Page extends BaseComponent {

    constructor() {
        super();
        this.selectors = SELECTORS;
        this.multiSelectors = MULTISELECTORS;
        this.installGetters({ selectors: SELECTORS, type: 'single' });
        this.installGetters({ selectors: MULTISELECTORS, type: 'multi' });
    }
    /**
    * Opens a sub page of the page
    * @param path path of the sub page (e.g. /path/to/page.html)
    */
    open(path) {
        return browser.url(`${path}`)
    }
}
