

import Page from './page'

/**
 * sub page containing specific selectors and methods for a specific page
 */
const SELECTORS = {
    inputUsername: '#username'
};
const MULTISELECTORS = {};
class LoginPage extends Page {
    constructor() {
        super();
        this.installGetters({ selectors: SELECTORS, type: 'single' });
        this.installGetters({ selectors: MULTISELECTORS, type: 'multi' });
    }
    /**
     * define selectors using getter methods
     */
    get inputPassword() {
        return $('#password');
    }

    get btnSubmit() {
        return $('button[type="submit"]');
    }

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    async login(username, password) {
        await this.inputUsername.setValue(username);
        await this.inputPassword.setValue(password);
        await this.btnSubmit.waitAndClick();
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    async open() {
        return super.open('login');
    }
}

export default new LoginPage();
