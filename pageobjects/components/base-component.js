const SELECTORS = {};
const MULTISELECTORS = {};
export default class BaseComponet {
    constructor() {
        this.installGetters({ selectors: SELECTORS, type: "single" });
        this.installGetters({ selectors: MULTISELECTORS, type: "multi" });
    }

    async installGetters({ selectors, type }) {
        for await (const key of Object.keys(selectors)) {
            if (type === "multi") {
                Object.defineProperty(this, key, {
                    get() {
                        return $$(selectors[key]);
                    },
                });
            } else {
                Object.defineProperty(this, key, {
                    get() {
                        return $(selectors[key]);
                    },
                });
            }
        }
    }
}
