module.exports = {
    elementScope: {
        waitAndClick: async function () {
            await this.waitForDisplayed()
            await this.click()
        }
    },
    browserScope: {
        waitThenClick: async function (element) {
            await element.waitForExist()
            await element.waitForDisplayed()
            await element.click()
        }
    }
}